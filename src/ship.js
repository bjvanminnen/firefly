const SHIP_IMAGE_WIDTH = 136;
const SHIP_IMAGE_HEIGHT = 136;
const SHIP_WIDTH = 48;
const SHIP_HEIGHT = 28;

const NO_FLY = {
  top: 25,
  bottom: 25,
  left: 25,
  right: 25
};


export default class Ship {
  constructor(firefly) {
    this.firefly = firefly;
    this.game = firefly.game;
    this.cursors = firefly.cursors;
    this.explosions = firefly.explosions;
    this.ship = null; // should arguably be called sprite instead

    // TODO - this is hacky
    this.endGame = firefly.end.bind(firefly);

    this.create_();
  }

  create_() {
    const ship = this.game.add.sprite(SHIP_IMAGE_WIDTH, SHIP_IMAGE_HEIGHT, 'ship');
    ship.width = SHIP_WIDTH;
    ship.height = SHIP_HEIGHT;
    this.game.physics.enable(ship, Phaser.Physics.ARCADE);
    ship.body.drag.x = 100;
    ship.body.drag.y = 40;
    ship.body.maxVelocity.x = 300;
    ship.body.maxVelocity.y = 300;
    ship.position.y = 300;

    this.ship = ship;
  }

  update() {
    const {game, cursors, ship} = this;

    if (!this.ship.exists) {
      return;
    }

    if (cursors.up.isDown) {
      ship.body.velocity.y -= 10
    }
    if (cursors.down.isDown) {
      ship.body.velocity.y += 10
    }
    if (cursors.left.isDown) {
      ship.body.velocity.x -= 10
    } else if (cursors.right.isDown) {
      ship.body.velocity.x += 10
    }

    ship.x = Math.max(ship.x, NO_FLY.left);
    ship.x = Math.min(ship.x, game.width - Math.abs(ship.width) - NO_FLY.right);
    ship.y = Math.max(ship.y, NO_FLY.top);
    ship.y = Math.min(ship.y, game.height - ship.height - NO_FLY.bottom);

    this.checkCollision_();
  }

  reverse() {
    this.ship.body.velocity.x = -this.ship.body.velocity.x;
    this.ship.anchor.setTo(0.5, 0);
    this.ship.scale.x = -this.ship.scale.x;
  }

  checkCollision_() {
    if (!this.ship.exists) {
      return;
    }

    // Did the ship hit the ground.
    if (this.ship.y > 380) {
      return this.shipCollision_();
    }

    // TODO - more allowing bounding box
    // Did the ship hit reavers
    this.firefly.reavers.getSprites().forEach(sprite => {
      // TODO - i think the reason collide wasnt working might be bc we were
      // setting x/y on sprite rather than body
      if (this.game.physics.arcade.intersects(this.ship.body, sprite.body)) {
        this.shipCollision_(sprite);
      }
    });

    // Did the ship get the gift
    const gift = this.firefly.gift;
    if (this.game.physics.arcade.intersects(this.ship.body, gift.body)) {
      this.firefly.acquiredGift();
    }
  }

  shipCollision_(target) {
    const explosion = this.explosions.getFirstExists(false);
    explosion.reset(this.ship.x, this.ship.y);
    explosion.play('kaboom', 30, false, true);
    this.firefly.sounds.explosion.play();
    this.ship.kill();
    if (target) {
      target.kill();
    }

    setTimeout(() => {
      this.endGame();
    }, 300);

    // TODO - game over experience
  }
}
