const REAVER_IMAGE_WIDTH = 229;
const REAVER_IMAGE_HEIGHT = 153;
const REAVER_WIDTH = 48;
const REAVER_HEIGHT = 32;
const REAVER_SLOPE = 1 / 5;
const REAVER_VELOCITY_X = 4 * 60;
const REAVER_VELOCITY_Y = 3 * 60;

// TODO - better reaver hit boxes

export default class Reavers {
  constructor(firefly, n) {
    this.list = [];
    this.game = firefly.game;

    this.create_(n);
  }

  getSprites() {
    return this.list;
  }

  create_(n=1) {
    for (var i = 0; i < n; i++) {
      const reaver = this.game.add.sprite(REAVER_IMAGE_WIDTH, REAVER_IMAGE_HEIGHT, 'reaver');
      reaver.width = REAVER_WIDTH;
      reaver.height = REAVER_HEIGHT;
      reaver.angle = 15;
      reaver.position.x = this.game.rnd.integerInRange(-REAVER_HEIGHT, this.game.width);
      reaver.position.y = this.game.rnd.integerInRange(-1200, 0);

      this.game.physics.enable(reaver, Phaser.Physics.ARCADE);

      this.list.push(reaver);
    }
  }

  reverse(groundVelocity) {
    this.list.forEach(reaver => {
      reaver.body.velocity.x += groundVelocity * 60;
    });
  }

  update() {
    this.list.forEach(reaver => {
      const MODIFIER = 0.2;

      const baseX = REAVER_VELOCITY_X * REAVER_SLOPE;
      const deltaX = Math.ceil(baseX * MODIFIER);
      const deltaY = Math.ceil(REAVER_VELOCITY_Y * MODIFIER);

      if (reaver.body.velocity.x === 0) {
        reaver.body.velocity.x = REAVER_VELOCITY_X * REAVER_SLOPE +
          this.game.rnd.integerInRange(-deltaX, deltaX);
        reaver.body.velocity.y = REAVER_VELOCITY_Y +
          this.game.rnd.integerInRange(-deltaY, deltaY);
      }

      this.resetIfOutOfBounds_(reaver);
    });
  }

  resetIfOutOfBounds_(reaver) {
    if (reaver.x > this.game.width || reaver.y > this.game.height) {
      reaver.position.x = this.game.rnd.integerInRange(-100, this.game.width);
      reaver.position.y = 0;
    }

  }
}
