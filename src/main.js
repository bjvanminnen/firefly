import Firefly from './firefly';

let firefly = new Firefly();

let button = document.getElementById('restart');
button.addEventListener('click', restart);

 function restart() {
  document.getElementById('firefly').innerHTML = null;
  firefly = new Firefly();
}
