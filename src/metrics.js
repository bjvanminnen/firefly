export function debugInfo(val) {
  if (!document) {
    return;
  }
  const element = document.getElementById('debug');
  element.textContent = val;
}
