import {debugInfo} from './metrics';
import Ship from './ship';
import Reavers from './reavers';

window.config = window.config || {};

// TODO - would prefer to require Phaser
const Phaser = window.Phaser;

const BACKGROUND_WIDTH = 400;
const BACKGROUND_HEIGHT = 300;
const BACKGROUND_VELOCITY = 5;

const GROUND_WIDTH = 800;
const GROUND_HEIGHT = 100;
const GROUND_VELOCITY = 3;

const NUM_REAVERS = config.numReavers || 6;
const GIFT_LOCATION = config.giftLocation || 2500;
const GIFT_SCREEN_LOCK = 700;

export default class Firefly {
  constructor() {
    const game = new Phaser.Game(800, 500, Phaser.AUTO, 'firefly', {
      preload: this.preload.bind(this),
      create: this.create.bind(this),
      update: this.update.bind(this)
    });
    this.game = game;

    this.pregame = true;

    this.cursors = null;
    this.reavers = null;
    this.ship = null;
    this.explosions = null;

    this.direction = 1;
    this.hasGift = false;
    this.done = false;

    this.distanceTravelled = 0;
  }

  preload() {
    const game = this.game;
    game.load.image('ship', 'assets/serenity.png');
    game.load.image('background', 'assets/starrysky.png');
    game.load.image('ground', 'assets/ground.png');
    game.load.image('reaver', 'assets/reaver.png');
    game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    game.load.image('wedding', 'assets/wedding.png');
    game.load.image('gift', 'assets/gift.png');
    game.load.image('ivan', 'assets/crazyivan.png');
    game.load.image('gray', 'assets/gray.png');
    game.load.image('startkey', 'assets/startkey.png');
    game.load.image('explosion', 'assets/explode.png');
    game.load.image('congrats', 'assets/congrats.png');

    game.load.audio('music', ['assets/FireflyTheme.mp3']);
    game.load.audio('cantdo', ['assets/somethingyoucantdo.mp3']);
    game.load.audio('explosion_sound', ['assets/explosion.mp3']);
    game.load.audio('gotgift', ['assets/gotgift.mp3']);
    game.load.audio('weddingmarch', ['assets/weddingmarch.mp3']);
  }

  create() {
    const game = this.game;

    this.sounds = {};

    this.sounds.music = game.add.audio('music');
    this.sounds.music.volume = 1;
    this.sounds.music.play();

    this.sounds.cantdo = game.add.audio('cantdo');

    this.sounds.explosion = game.add.audio('explosion_sound');
    this.sounds.explosion.volume = 0.8;

    this.sounds.gotgift = game.add.audio('gotgift');
    this.sounds.wedding = game.add.audio('weddingmarch');


    game.physics.startSystem(Phaser.Physics.ARCADE);

    game.stage.backgroundColor = '#888888';
    this.background = game.add.tileSprite(0, 0, BACKGROUND_WIDTH * 2,
      BACKGROUND_HEIGHT, "background");
    this.background.scale.x = game.width / this.background.width;
    this.background.scale.y = game.height / this.background.height;

    this.ground = game.add.tileSprite(0, game.height - GROUND_HEIGHT,
      GROUND_WIDTH, GROUND_HEIGHT, 'ground');
    game.physics.enable(this.ground, Phaser.Physics.ARCADE);

    this.wedding = game.add.sprite(80, 61, 'wedding');
    this.wedding.x = 50;
    this.wedding.y = 400;

    const gray = game.add.sprite(1, 1, 'gray');
    gray.width = 170;
    gray.height = 50;
    gray.x = 610;
    gray.y = 430;
    gray.alpha = 0.2;

    this.foundGift = game.add.sprite(61, 80, 'gift');
    this.foundGift.x = 740;
    this.foundGift.y = 435;
    this.foundGift.width = 30;
    this.foundGift.height = 40;
    this.foundGift.alpha = 0.4;

    this.cursors = game.input.keyboard.createCursorKeys();

    this.explosions = game.add.group();
    this.explosions.createMultiple(10, 'kaboom');
    this.explosions.forEach(item => {
      item.anchor.x = 0.2;
      item.anchor.y = 0.5;
      item.animations.add('kaboom');
    });

    this.ivan = game.add.sprite(484, 124, 'ivan');
    this.ivan.alpha = 0.4;
    this.ivan.width = 100;
    this.ivan.height = 26;
    this.ivan.x = 620;
    this.ivan.y = 445;
    this.ivan.inputEnabled = true;
    this.ivan.events.onInputDown.add(() => this.handleCrazyIvan());

    var key = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    key.onDown.add(() => this.handleCrazyIvan());

    this.startkey = game.add.sprite(483, 40, 'startkey');
    this.startkey.x = 170;
    this.startkey.y = 200;

    this.congrats = game.add.sprite(490, 50, 'congrats');
    this.congrats.x = 170;
    this.congrats.y = 200;
    this.congrats.visible = false;

    this.reavers = new Reavers(this, NUM_REAVERS);
    this.ship = new Ship(this);

    this.gift = game.add.sprite(61, 80, 'gift');
    this.gift.x = this.ship.ship.x + GIFT_LOCATION;
    this.gift.y = 250;
    this.gift.width = 30;
    this.gift.height = 40;
    game.physics.enable(this.gift, Phaser.Physics.ARCADE);

    this.distanceText = game.add.text(5, 5, GIFT_LOCATION + ' m', {
      font: '20px Arial',
      fill: 'yellow'
    });
    this.updateDistance();
  }

  update() {
    if (this.done) {
      return;
    }

    if (this.pregame) {
      if (this.cursors.up.isDown || this.cursors.down.isDown ||
          this.cursors.left.isDown || this.cursors.right.isDown ||
          this.game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR)) {
        this.pregame = false;
        this.startkey.kill();
        this.game.add.tween(this.sounds.music).to({ volume: 0.2 }, 6000).start();
      } else {
        return;
      }
    }

    this.ship.update();
    this.reavers.update(-GROUND_VELOCITY * this.direction);

    this.background.tilePosition.x -= BACKGROUND_VELOCITY * this.direction;
    this.ground.tilePosition.x -= GROUND_VELOCITY * this.direction;
    this.wedding.x -= GROUND_VELOCITY * this.direction;

    if (this.gift.x > GIFT_SCREEN_LOCK) {
      this.gift.x -= GROUND_VELOCITY;
    }

    if (this.hasGift && this.ship.ship.x < (this.wedding.x + this.wedding.width)) {
      this.end(true);
    }

    this.updateDistance();
  }

  updateDistance() {
    let item;
    let distance;
    if (!this.hasGift) {
      item = 'gift';
      distance = this.gift.x - this.ship.ship.x;
    } else {
      item = 'wedding';
      distance = this.ship.ship.x - (this.wedding.x + this.wedding.width);
    }
    distance = Math.max(0, Math.round(distance));
    this.distanceText.text = `Distance to ${item}: ${distance} m`;
  }

  acquiredGift() {
    if (this.hasGift) {
      return;
    }
    this.sounds.gotgift.play();
    this.hasGift = true;
    this.gift.kill();
    this.foundGift.alpha = 1;
    this.ivan.alpha = 1;
  }

  handleCrazyIvan() {
    if (!this.hasGift) {
      return;
    }

    this.sounds.cantdo.volume = 4;
    this.sounds.cantdo.play();
    setTimeout(() => {
      this.direction = -1;
      this.ship.reverse();
      this.ivan.alpha = 0.4;
      this.foundGift.alpha = 0.4
      this.reavers.reverse(GROUND_VELOCITY);
    }, 450);

    setTimeout(() => {
      this.sounds.cantdo.volume = 0;
    }, 1900);
  }

  end(won=false) {
    this.done = true;
    if (won) {
      this.sounds.wedding.play();
      this.sounds.music.volume = 0;
      this.congrats.visible = true;
      this.ship.ship.body.velocity.x = 0;
      this.ship.ship.body.velocity.y = 0;
      this.game.add.tween(this.ship.ship).to({
        x: this.wedding.x,
        y: this.wedding.y
      }, 1000).start();
    }
  }
}
