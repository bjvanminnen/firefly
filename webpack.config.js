var webpack = require('webpack');

var config = module.exports = {
  entry: {
    main: [
      __dirname + '/node_modules/babel-core/browser-polyfill.js',
      './src/main.js'
    ]
  },
  output: {
    filename: 'build/[name].js'
  },
  module: {
    loaders: [
      {
        test: /src\/.*\.jsx*$/,
        loaders: [
          'babel-loader?stage=0'
        ]
      },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      }
   ],
    noParse : [
      /\/babel-core\/browser-polyfill\.js$/
    ],
  },
  resolve: {
    extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx"]
  },
  devtool: "#inline-source-map",
  // devtool: "eval-source-map"
}

if (process.env.PROD_BUILD) {
  config.devtool = undefined;
  config.plugins = config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      minimize: true,
      sourceMap: false,
      compress: {
        warnings: false
      }
    })
  );
}
